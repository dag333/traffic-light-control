function [tt1, tt2, tt3] = CompareAlg2( time, iters, in_cars )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Travel_N_1 = zeros(1,iters);
Travel_N_2 = zeros(1,iters);
Travel_A = zeros(1,iters);

ttmean1 = zeros(1,iters);
ttmean2 = zeros(1,iters);
ttmean3 = zeros(1,iters);


add_cars = false;
max_cars = 0;

tt1 = zeros(1, in_cars);
tt2 = zeros(1, in_cars);
tt3 = zeros(1, in_cars);


for cars=1:in_cars
    
    for x=1:iters
        
        [Cars, Roads, Intersections, TIME] = generateModel(cars);
        
        save('Sim.mat', 'Cars', 'Intersections', 'Roads', 'TIME','time');
        
        [ Cars, Roads, Intersections, TIME] = Simulation( Cars, Roads, Intersections, TIME, time, add_cars, max_cars, 1 );
        
        travelingtime1 = zeros(1, length(Cars));
        
        for i=1:length(Cars)
            travelingtime1(i) = Cars{i}.tt;
        end
        
        ttmean1(x) = mean(travelingtime1);
        Travel_N_1(x) = ttmean1(x);
        
        
        
        load('Sim.mat')
        
        [ Cars, Roads, Intersections, TIME] = Simulation( Cars, Roads, Intersections, TIME, time, add_cars, max_cars, 2 );
        
        travelingtime2 = zeros(1, length(Cars));
        
        for i=1:length(Cars)
            travelingtime2(i) = Cars{i}.tt;
        end
        
        ttmean2(x) = mean(travelingtime2);
        Travel_N_2(x) = ttmean2(x);
        
        
        
        load('Sim.mat')
        
        
        [ Cars, Roads, Intersections, TIME] = SimulationSmart( Cars, Roads, Intersections, TIME, time, add_cars, max_cars );
        
        travelingtime3 = zeros(1, length(Cars));
        
        for i=1:length(Cars)
            travelingtime3(i) = Cars{i}.tt;
        end
        
        ttmean3(x) = mean(travelingtime3);
        Travel_A(x) = ttmean3(x);
        
        
        
        %         X = zeros(1,time);
        %
        %         for i=1:time
        %             X(i) = i;
        %         end
        %
        %         %      figure;
        %         %      plot(X, wait1, X, wait2);
        %
        % %         ttmean1 = mean(travelingtime1);
        % %         ttmean2 = mean(travelingtime2);
        % %         ttmean3 = mean(travelingtime3);
        % %         means = [ttmean1 ttmean2 ttmean3];
        %
        %         %figure;
        %         %bar(means);
        
    end
    
    MeanTrav_N_1 = mean(Travel_N_1);
    MeanTrav_N_2 = mean(Travel_N_2);
    MeanTrav_A = mean(Travel_A);
    
    tt1(cars) = MeanTrav_N_1;
    tt2(cars) = MeanTrav_N_2;
    tt3(cars) = MeanTrav_A;
    
end

X = zeros(1, in_cars);

for i=1:in_cars
    X(i) = i;
end


figure;
plot(X, tt1, X, tt2, X, tt3);

title('Comparison of Traffic Light Controllers');
xlabel('Number of Cars');
ylabel('Average Traveling Time (timesteps)');
legend('Non-Adapative (1s)','Non-Adapative (2s)', 'Adaptive');


end
