function [MeanTrav1, MeanTrav2, MeanTrav3, MeanWait1, MeanWait2, MeanWait3] = CompareAlg( time, num_cars, iters, add_cars, max_cars )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Travel_1 = [];
Travel_2 = [];
Travel_3 = [];

Wait_1 = [];
Wait_2 = [];
Wait_3 = [];


cars = num_cars;

for x=1:iters
    
    [Cars, Roads, Intersections, TIME] = generateModel(cars);
    
    save('Sim.mat', 'Cars', 'Intersections', 'Roads', 'TIME', 'add_cars', 'cars', 'iters','max_cars', 'num_cars','time');
    
    [ Cars, Roads, Intersections, TIME, wait1] = Simulation( Cars, Roads, Intersections, TIME, time, add_cars, max_cars, 1 );
    
    travelingtime1 = zeros(1, length(Cars));
    
    for i=1:length(Cars)
        travelingtime1(i) = Cars{i}.tt;
    end
    
    ttmean1(x) = mean(travelingtime1);
    Travel_1(x) = ttmean1(x);
    
    wmean1(x) = mean(wait1);
    Wait_1(x) = wmean1(x);
    
    load('Sim.mat')
    
    [ Cars, Roads, Intersections, TIME, wait2] = Simulation( Cars, Roads, Intersections, TIME, time, add_cars, max_cars, 2 );
    
    travelingtime2 = zeros(1, length(Cars));
    
    for i=1:length(Cars)
        travelingtime2(i) = Cars{i}.tt;
    end
    
    ttmean2(x) = mean(travelingtime2);
    Travel_2(x) = ttmean2(x);
    
    wmean2(x) = mean(wait2);
    Wait_2(x) = wmean2(x);
    
    load('Sim.mat')
    
    
    [ Cars, Roads, Intersections, TIME, wait3 ] = SimulationSmart( Cars, Roads, Intersections, TIME, time, add_cars, max_cars );
    
    travelingtime3 = zeros(1, length(Cars));
    
    for i=1:length(Cars)
        travelingtime3(i) = Cars{i}.tt;
    end
    
    ttmean3(x) = mean(travelingtime3);
    Travel_3(x) = ttmean3(x);
    
    wmean3(x) = mean(wait3);
    Wait_3(x) = wmean2(x);
    
    X = zeros(1,time);
    
    for i=1:time
        X(i) = i;
    end
    
%       figure;
%       plot(X, wait1, X, wait2, X, wait3);
    
    ttmean1 = mean(travelingtime1);
    ttmean2 = mean(travelingtime2);
    ttmean3 = mean(travelingtime3);
    means = [ttmean1 ttmean2 ttmean3];
    
    %figure;
    %bar(means);
    
end

MeanTrav1 = mean(Travel_1);
MeanTrav2 = mean(Travel_2);
MeanTrav3 = mean(Travel_3);
MeanWait1 = mean(Wait_1);
MeanWait2 = mean(Wait_2);
MeanWait3 = mean(Wait_3);






end
