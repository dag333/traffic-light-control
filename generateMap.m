function [ map, error ] = generateMap( Cars, i )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

nextInter = nextIntersection(Cars{i}.position);
lastInter = lastIntersection(Cars{i}.position);

error = false;
map = [0 0 0 0];

if(nextInter == 1)
    curr_x = 1;
    curr_y = 2;
elseif(nextInter == 2)
    curr_x = 2;
    curr_y = 2;
elseif(nextInter == 3)
    curr_x = 2;
    curr_y = 1;
elseif(nextInter == 4)
    curr_x = 1;
    curr_y = 1;
else
    curr_x = 0;
    curr_y = 0;
end

if(lastInter == 1)
    last_x = 1;
    last_y = 2;
elseif(lastInter == 2)
    last_x = 2;
    last_y = 2;
elseif(lastInter == 3)
    last_x = 2;
    last_y = 1;
elseif(lastInter == 4)
    last_x = 1;
    last_y = 1;
elseif(lastInter == 11)
    last_x = 1;
    last_y = 3;
elseif(lastInter == 12)
    last_x = 2;
    last_y = 3;
elseif(lastInter == 13)
    last_x = 3;
    last_y = 2;
elseif(lastInter == 14)
    last_x = 3;
    last_y = 1;
elseif(lastInter == 15)
    last_x = 2;
    last_y = 0;
elseif(lastInter == 16)
    last_x = 1;
    last_y = 0;
elseif(lastInter == 17)
    last_x = 0;
    last_y = 1;
elseif(lastInter == 18)
    last_x = 0;
    last_y = 2;
end

if(curr_x ~= 0)
    [dest_x, dest_y] = findDest(Cars, i);
    %finds destination index on x,y coordinate plane
    
    x = dest_x - curr_x;
    y = dest_y - curr_y;
    
    
    while((x ~= 0) || (y~=0))
        p = round(rand()); % random 0 or 1
        
        if(p == 0 || y == 0 )
            if(x > 0) %moving to the right
                inter = xyInt(curr_x,curr_y);
                
                if(inter == 0)
                    error = true;
            %        disp('error1');
                    break;
                end
                
                if(last_x == curr_x)  %moving vert
                    map(inter) = 1;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    x = x - 1;
                    curr_x = curr_x + 1;
                    
                elseif(last_y == curr_y) %moving horiz
                    map(inter) = 0;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    x = x - 1;
                    curr_x = curr_x + 1;
                end
                
                
                
            elseif( x < 0 ) %moving to the left
                inter = xyInt(curr_x,curr_y);
                
                if(inter == 0)
                    error = true;
              %      disp('error2');
                    break;
                end
                
                if(last_x == curr_x)  %moving vert
                    map(inter) = -1;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    x = x + 1;
                    curr_x = curr_x - 1;
                    
                elseif(last_y == curr_y) %moving horiz
                    map(inter) = 0;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    x = x + 1;
                    curr_x = curr_x - 1;
                end
                
            else %if x==0
                % inter = xyInt(curr_x,curr_y);
                
                % map(inter) = 0;
                
            end
        elseif(p == 1 || x == 0 )  %if p == 1
            if(y > 0) %moving up
                inter = xyInt(curr_x,curr_y);
                
                if(inter == 0)
                    error = true;
                %    disp('error3');
                    break;
                end
                
                if(last_x == curr_x)  %moving vert
                    map(inter) = 0;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    
                    y = y - 1;
                    curr_y = curr_y + 1;
                    
                    
                elseif(last_y == curr_y) %moving horiz
                    map(inter) = -1;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    
                    y = y - 1;
                    curr_y = curr_y + 1;
                end
                
                
            elseif( y < 0 ) %moving down
                inter = xyInt(curr_x,curr_y);
                
                if(inter == 0)
                    error = true;
                  %  disp('error4');
                    break;
                end
                
                
                if(last_x == curr_x)  %moving vert
                    map(inter) = 0;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    
                    y = y + 1;
                    curr_y = curr_y - 1;
                    
                    
                elseif(last_y == curr_y) %moving horiz
                    map(inter) = 1;
                    
                    last_x = curr_x;
                    last_y = curr_y;
                    
                    
                    y = y + 1;
                    curr_y = curr_y - 1;
                end
                
            else %if y==0
                %  inter = xyInt(curr_x,curr_y);
                
                %  map(inter) = 0;
            end
            
            
        end
        
    end
    
    if(error == true)
        [ map, error ] = generateMap( Cars, i );
    end
    
end
end

