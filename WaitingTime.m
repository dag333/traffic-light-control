function [ w1, w2, w3, w4 ] = WaitingTime( Cars )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here


[ q1, q2, q3, q4 ] = generateQueues( Cars );

w1=0;
w2=0;
w3=0;
w4=0;

for i = q1
   
  w1 = w1 + Cars{i}.wait;  
    
end

for i = q2
   
  w2 = w2 + Cars{i}.wait;  
    
end

for i = q3
   
  w3 = w3 + Cars{i}.wait;  
    
end

for i = q4
   
  w4 = w4 + Cars{i}.wait;  
    
end


end

