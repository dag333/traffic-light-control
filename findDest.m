function [dest_x, dest_y] = findDest(Cars, i)
%Given a random num from 1 to 8
%convert to x,y coordinate for generating map

num = Cars{i}.dest_num;


if(num == 1)
    
    dest_x = 1;
    dest_y = 3;
    
elseif(num == 2)
    
    dest_x = 2;
    dest_y = 3;
    
elseif(num == 3)
    
    dest_x = 3;
    dest_y = 2;  
    
elseif(num == 4)
    
    dest_x = 3;
    dest_y = 1;    
    
elseif(num == 5)
    
    dest_x = 2;
    dest_y = 0;
    
elseif(num == 6)
    
    dest_x = 1;
    dest_y = 0;
    
elseif(num == 7)
    
    dest_x = 0;
    dest_y = 1;

elseif(num == 8)
    
    dest_x = 0;
    dest_y = 2;
    
end

