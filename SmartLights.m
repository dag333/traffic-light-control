function [ Intersections ] = SmartLights( Intersections, Cars, Roads )
%SMARTLIGHTS 
%   changes the traffic lights using RL algorithm


[ q1, q2, q3, q4, q5, q6, q7, q8] = generateQueues( Cars );

q_1_on = 0;
q_1_off = 0;
q_2_on = 0;
q_2_off = 0;
q_3_on = 0;
q_3_off = 0;
q_4_on = 0;
q_4_off = 0;
q_5_on = 0;
q_5_off = 0;
q_6_on = 0;
q_6_off = 0;
q_7_on = 0;
q_7_off = 0;
q_8_on = 0;
q_8_off = 0;

recur = 0;
for i=q1 %queue for Intersection 1
    
    q_1_off = q_1_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_1_on = q_1_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q2
    
    q_2_off = q_2_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_2_on = q_2_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q3
    
    q_3_off = q_3_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_3_on = q_3_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q4
    
    q_4_off = q_4_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_4_on = q_4_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q5
    
    q_5_off = q_5_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_5_on = q_5_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q6
    
    q_6_off = q_6_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_6_on = q_6_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q7
    
    q_7_off = q_7_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_7_on = q_7_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
    
end

recur = 0;
for i=q8
    
    q_8_off = q_8_off + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 0, recur );
    q_8_on = q_8_on + Q( Cars, Roads, Intersections, i, Cars{i}.position, Cars{i}.dest, 1, recur );
   
end



i1_1 = q_1_off + q_2_on;
i1_2 = q_1_on + q_2_off;

i2_1 = q_3_off + q_4_on;
i2_2 = q_3_on + q_4_off;

i3_1 = q_5_off + q_6_on;
i3_2 = q_5_on + q_6_off;

i4_1 = q_7_off + q_8_on;
i4_2 = q_7_on + q_8_off;


if(i1_1 > i1_2)
   Intersections{1}.light = [1 0];
elseif(i1_1 < i1_2)
    Intersections{1}.light = [0 1];
end

if(i2_1 > i2_2)
   Intersections{2}.light = [1 0];
elseif(i2_1 < i2_2)
    Intersections{2}.light = [0 1];
end

if(i3_1 > i3_2)
   Intersections{3}.light = [1 0];
elseif(i3_1 < i3_2)
    Intersections{3}.light = [0 1];
end

if(i4_1 > i4_2)
   Intersections{4}.light = [1 0];
elseif(i4_1 < i4_2)
    Intersections{4}.light = [0 1];
end




end

