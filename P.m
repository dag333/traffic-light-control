function [ p ] = P( Cars, Roads, Intersections, pos, i, newpos, light )
%Creates P matrix
%calculating probability of car's position in next timestep


%PROBABILITIES
%velocity1
a = .3; %prob staying in same position
b = 1 - a; %prob moving 1;
%velocity2
c = .2; %prob staying in same position
d = .1; %prob moving 1 cell
e = 1 - c-d; %prob moving 2 cells


if(pos(2) == 1)
    direction = 1;
else
    direction = -1;
end

p = 0;

%if car is not at an intersection
if(atIntersection( Cars, Roads, i ) == 0)
    
    if(Cars{i}.velocity == 1)
        
        if(  isequal(newpos, pos) )
            p = a;
        elseif( isequal(newpos, [ pos(1) pos(2) pos+direction ]))
            p = b;
        else
            p = 0;
        end
        
    elseif(Cars{i}.velocity == 2)
        
        if(  isequal(newpos, pos) )
            p = c;
        elseif( isequal( newpos, [ pos(1) pos(2) pos(3)+direction ]))
            p = d;
        elseif( isequal(newpos, [ pos(1) pos(2) pos(3)+2*direction ]))
            p = e;
        else
            p = 0;
        end
        
    end
    
    %if car is at an intersection
else
    inter_num = atIntersectionSim( pos, Cars, Roads, i );
    
    %    road = pos(1);
    
    %     %finding the status of the correct traffic light
    %     if(Intersections{inter_num}.position(1) == road)
    %         light = Intersections{inter_num}.light(1);
    %     elseif(Intersections{inter_num}.position(2) == road)
    %         light = Intersections{inter_num}.light(2);
    %     end
    
    %how the car with act at the current intersection
    movement = Cars{i}.map(inter_num);
    
    
    if(light == 0) %red
        
        if( isequal(newpos, pos) )
            p = 1;
        else
            p = 0;
        end
        
    else
        
        if(movement == 0)
            
            if(Cars{i}.velocity == 1)
                
                if( isequal(newpos, pos) )
                    p = a;
                elseif( isequal(newpos, [ pos(1) pos(2) pos(3)+direction ]))
                    p = b;
                else
                    p = 0;
                end
                
            elseif(Cars{i}.velocity == 2)
                
                if( isequal(newpos, pos) )
                    p = c;
                elseif( isequal(newpos, [ pos(1) pos(2) pos(3)+direction ]))
                    p = d;
                elseif( isequal(newpos, [ pos(1) pos(2) pos(3)+2*direction ]))
                    p = e;
                else
                    p = 0;
                end
            end
            
        elseif(movement == -1)
            
            Road1 = Intersections{inter_num}.position(1);
            Road2 = Intersections{inter_num}.position(2);
            
            
            if(pos(1) == Road1)
                
                if(Roads{Road2}.inter(2,1) == inter_num)
                    newcell = Roads{Road2}.inter(1,1);
                else
                    newcell = Roads{Road2}.inter(1,2);
                end
                
                if( isequal(newpos, pos) )
                    p = a;
                elseif( isequal(newpos, [ Road2 2 newcell-1 ]) )
                    p = b;
                else
                    p = 0;
                end
                
            else
                if(Roads{Road1}.inter(2,1) == inter_num)
                    newcell = Roads{Road1}.inter(1,1);
                else
                    newcell = Roads{Road1}.inter(1,2);
                end
                
                
                if( isequal(newpos, pos) )
                    p = a;
                elseif( isequal(newpos, [ Road1 2 newcell-1 ]))
                    p = b;
                else
                    p = 0;
                end
                
            end
            
        elseif(movement == 1)
            
            Road1 = Intersections{inter_num}.position(1);
            Road2 = Intersections{inter_num}.position(2);
            
            
            if(pos(1) == Road1)
                
                if(Roads{Road2}.inter(2,1) == inter_num)
                    newcell = Roads{Road2}.inter(1,1);
                else
                    newcell = Roads{Road2}.inter(1,2);
                end
                
                
                if( isequal(newpos, pos) )
                    p = a;
                elseif( isequal(newpos, [ Road2 1 newcell+1 ]) )
                    p = b;
                else
                    p = 0;
                end
                
                
            else
                if(Roads{Road1}.inter(2,1) == inter_num)
                    newcell = Roads{Road1}.inter(1,1);
                else
                    newcell = Roads{Road1}.inter(1,2);
                end
                
                if( isequal(newpos, pos) )
                    p = a;
                elseif( isequal(newpos, [ Road1 1 newcell+1 ]) )
                    p = b;
                else
                    p = 0;
                end
                
                
            end
        end
        
        
    end
    
end





end

