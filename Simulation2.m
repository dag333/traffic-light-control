function [ Cars, Roads, Intersections, counter  ] = Simulation2( Cars, Roads, Intersections, time )
%Simulation of Cars and Roads

counter = 1;

for t=1:time
    for r=1:length(Roads)
        for l=1:length(Roads{r}.lane)
            if(Roads{r}.lane{l}.index == 1)  %if in lane 1 (lane 1 counts up)
                for c=length(Roads{r}.lane{l}.car_cells):-1:1
                    
                    if( Roads{r}.lane{l}.car_cells(c) ~= 0) %check if car is in cell
                        current = Roads{r}.lane{l}.car_cells(c);
                        
                        if( Cars{current}.active == true && Cars{current}.hasmoved == false) %check if car is in system
                            if(atIntersection(Cars, Roads, current) ~= false) %check if car is at intersection
                                intersection_num = atIntersection(Cars, Roads, current); %which intersection
                                
                                if( Intersections{intersection_num}.light(Cars{current}.position(2)) == 1) %if green
                                   disp('sim20');
                                    [ Cars, Roads ] = turn( Cars, Roads, Intersections, intersection_num, current );
                                end
                            else
                                [Cars, Roads] = move(Cars, Roads, current); %move car
                                disp('sim25');
                                
                            end
                        end
                    end
                end
                
            else %if in lane 2 (counts down) ASSUMING ONLY TWO LANES
                for c=1:length(Roads{r}.lane{l}.car_cells)
                    if( Roads{r}.lane{l}.car_cells(c) ~= 0) %check if car is in cell
                        current = Roads{r}.lane{l}.car_cells(c);
                        
                        if( Cars{current}.active == true && Cars{current}.hasmoved == false) %check if car is in system
                            if(atIntersection(Cars, Roads, current) ~= false) %check if car is at intersection
                                intersection_num = atIntersection(Cars, Roads, current); %which intersection
                                
                                if( Intersections{intersection_num}.light(Cars{current}.position(2)) == 1) %if green
                                    [ Cars, Roads ] = turn( Cars, Roads, Intersections, intersection_num, current );
                                end
                            else
                                [Cars, Roads] = move(Cars, Roads, current); %move car
                                
                                
                            end
                        end
                    end
                end
            end
            
        end
    end
    
    
    %simple way change/alter the traffic lights
    
    % if(counter < 3)
    %      counter = counter + 1;
    % else
    %      for L=1:length(Intersections)
    %          if(Intersections{L}.light == [1 1 1])
    %               Intersections{L}.light = [0 0 0];
    %          else
    %               Intersections{L}.light = [1 1 1];
    %          end
    %      end
    %      counter = 1;
    % end
    
    for i=1:length(Cars)
        Cars{i}.hasmoved = false; 
    end    
    
end


end

