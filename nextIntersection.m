function [ next_int ] = nextIntersection( pos )
%nextIntersection: given a car's location, determines the
%upcoming intersection


road = pos(1);
lane = pos(2);
cell = pos(3);

next_int = 0;

if(road == 1)
    if(lane == 1)
        if(cell <= 3)
            next_int = 1;
        elseif((cell>3)&&(cell<9))
            next_int = 2;
        else
            next_int = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            next_int = 0;
        elseif((cell >= 3)&&(cell<8))
            next_int = 1;
        else
            next_int = 2;
        end
    end
elseif(road == 2)
    if(lane == 1)
        if(cell<= 3)
            next_int = 4;
        elseif((cell>3)&&(cell<9))
            next_int = 3;
        else
            next_int = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            next_int = 0;
        elseif((cell >= 3)&&(cell<8))
            next_int = 4;
        else
            next_int = 3;
        end
    end
elseif(road == 3)
    if(lane == 1)
        if(cell <= 3)
            next_int = 1;
        elseif((cell>3)&&(cell<9))
            next_int = 4;
        else
            next_int = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            next_int = 0;
        elseif((cell>=3)&&(cell<8))
            next_int = 1;
        else
            next_int = 4;
        end
    end
elseif(road == 4)
    if(lane == 1)
        if(cell <= 3)
            next_int = 2;
        elseif((cell>3)&&(cell<9))
            next_int = 3;
        else
            next_int = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            next_int = 0;
        elseif((cell>=3)&&(cell<8))
            next_int = 2;
        else
            next_int = 3;
        end
    end
end
end

