figure;
plot([4;4],[0;24]);hold on;
plot([6;6],[0;24], '--k');hold on;
plot([8;8],[0;24]);hold on;
plot([16;16],[0;24]);hold on;
plot([18;18],[0;24], '--k');hold on;
plot([20;20],[0;24]);hold on;

plot([0;24],[4;4]);hold on;
plot([0;24],[6;6], '--k');hold on;
plot([0;24],[8;8]);hold on;
plot([0;24],[16;16]);hold on;
plot([0;24],[18;18], '--k');hold on;
plot([0;24],[20;20]);hold on;


for i=1:length(Cars)

    Road = Cars{i}.position(1);
    Lane = Cars{i}.position(2);
    Cell = Cars{i}.position(3);
    
    
    if(Road == 1)
       
        x = Cell*2 - 1;
        y = 18;
        
        if(Lane == 1)
           y = y-1;
        else
           y = y+1; 
        end
        
    elseif(Road == 2)
        
        x = Cell*2 - 1;
        y = 6;
        
        if(Lane == 1)
           y = y-1;
        else
           y = y+1; 
        end
        
    elseif(Road == 3)
        
        x = 6;
        y = 25 - Cell*2;
        
        if(Lane == 1)
           x = x-1;
        else
           x = x+1; 
        end
    
    else
        
        x = 18;
        y = 25 - Cell*2;
        
        if(Lane == 1)
           x = x-1;
        else
           x = x+1; 
        end  
    
    end
    
    
    plot(x,y,'--rs'); hold on;
    
    
    
end