function [ last_int ] = lastIntersection( pos )
%nextIntersection: given a car's location, determines the
%upcoming intersection


road = pos(1);
lane = pos(2);
cell = pos(3);

last_int = 0;

if(road == 1)
    if(lane == 1)
        if(cell <= 3)
            last_int = 18;
        elseif((cell>3)&&(cell<9))
            last_int = 1;
        else
            last_int = 2;
        end
    elseif(lane == 2)
        if(cell < 3)
            last_int = 1;
        elseif((cell >= 3)&&(cell<8))
            last_int = 2;
        else
            last_int = 13;
        end
    end
elseif(road == 2)
    if(lane == 1)
        if(cell<= 3)
            last_int = 17;
        elseif((cell>3)&&(cell<9))
            last_int = 4;
        else
            last_int = 3;
        end
    elseif(lane == 2)
        if(cell < 3)
            last_int = 4;
        elseif((cell >= 3)&&(cell<8))
            last_int = 3;
        else
            last_int = 14;
        end
    end
elseif(road == 3)
    if(lane == 1)
        if(cell <= 3)
            last_int = 11;
        elseif((cell>3)&&(cell<9))
            last_int = 1;
        else
            last_int = 4;
        end
    elseif(lane == 2)
        if(cell < 3)
            last_int = 1;
        elseif((cell>=3)&&(cell<8))
            last_int = 4;
        else
            last_int = 16;
        end
    end
elseif(road == 4)
    if(lane == 1)
        if(cell <= 3)
            last_int = 12;
        elseif((cell>3)&&(cell<9))
            last_int = 2;
        else
            last_int = 3;
        end
    elseif(lane == 2)
        if(cell < 3)
            last_int = 2;
        elseif((cell>=3)&&(cell<8))
            last_int = 3;
        else
            last_int = 15;
        end
    end
end
end

