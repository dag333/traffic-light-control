function [ q ] = InQueue( pos )
%nextIntersection: given a car's location, determines the
%upcoming intersection


road = pos(1);
lane = pos(2);
cell = pos(3);

q = 0;

if(road == 1)
    if(lane == 1)
        if((cell <= 3)&&(cell > 1))
            q = 1;
        elseif((cell>6)&&(cell<9))
            q = 3;
        else
            q = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            q = 0;
        elseif((cell >= 3)&&(cell<5))
            q = 1;
        elseif((cell == 8)||(cell==9))
            q = 3;
        end
    end
elseif(road == 2)
    if(lane == 1)
        if((cell <= 3)&&(cell > 1))
            q = 7;
        elseif((cell>6)&&(cell<9))
            q = 5;
        else
            q = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            q = 0;
        elseif((cell >= 3)&&(cell<5))
            q = 7;
        elseif((cell == 8)||(cell==9))
            q = 5;
        end
    end
elseif(road == 3)
    if(lane == 1)
        if((cell <= 3)&&(cell>1))
            q = 2;
        elseif((cell>6)&&(cell<9))
            q = 8;
        else
            q = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            q = 0;
        elseif((cell>=3)&&(cell<5))
            q = 2;
        elseif((cell==8)||(cell==9))
            q = 8;
        end
    end
elseif(road == 4)
    if(lane == 1)
        if((cell <= 3)&&(cell>1))
            q = 4;
        elseif((cell>6)&&(cell<9))
            q = 6;
        else
            q = 0;
        end
    elseif(lane == 2)
        if(cell < 3)
            q = 0;
        elseif((cell>=3)&&(cell<5))
            q = 4;
        elseif((cell==8)||(cell==9))
            q = 6;
        end
    end
end
end

