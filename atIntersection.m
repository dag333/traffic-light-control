function [ output, move_one ] = atIntersection( Cars, Roads, i )
%intersection Takes in cars and current index
%   return which intersection car is at, if none returns 0.

output = false;
move_one = false;

if(Cars{i}.position(2) == 2) %if in lane 2, moves up
    direction = 1;
elseif(Cars{i}.position(2) == 1) %if in lane 1, moves down
    direction = -1;
end


int_cell_1 = Roads{Cars{i}.position(1)}.inter(1,1);
int_cell_2 = Roads{Cars{i}.position(1)}.inter(1,2);



if( Roads{Cars{i}.position(1)}.inter(1,1) == Cars{i}.position(3)) 
     output = Roads{Cars{i}.position(1)}.inter(2,1);   
%     sprintf('if')
    
elseif(Roads{Cars{i}.position(1)}.inter(1,2) == Cars{i}.position(3))
    output = Roads{Cars{i}.position(1)}.inter(2,2);
%    sprintf('elseif1')
    
elseif((Cars{i}.velocity == 2)&&(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(int_cell_1) == 0)&&(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(int_cell_1+direction) == Cars{i}.index))    
    output = Roads{Cars{i}.position(1)}.inter(2,1);
    move_one = true;
    
%    sprintf('elseif2')

elseif((Cars{i}.velocity == 2)&&(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(int_cell_2) == 0)&&(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(int_cell_2+direction) == Cars{i}.index))    
    output = Roads{Cars{i}.position(1)}.inter(2,2);
    move_one = true;

%    sprintf('elseif3')

else
%    sprintf('else')
    
end