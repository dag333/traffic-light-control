function [ Cars, Roads ] = move( Cars, Roads, i )
%move: increments car. checks surroundings before moving
% if outside of system, sets to non-active


%defining direction by lane number
if(Cars{i}.position(2) == 2) %if in lane 2, moves up
    direction = -1;
elseif(Cars{i}.position(2) == 1) %if in lane 1, moves down
    direction = 1;
end

%if(Cars{i}.hasmoved == false)
    
    % ASSUMING CAR VELOCITY = 1 OR 2 CELLS/TIME STEP
    if ( (Cars{i}.position(3) + direction*Cars{i}.velocity < length(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells))&&(Cars{i}.position(3) + direction*Cars{i}.velocity >0)) % check if going outside of system
        if(( Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3) + direction*Cars{i}.velocity) == 0) && (Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3) + direction*1) == 0)) %does car exist in next cell (& step for Cars moving 2)
            
            
            %no cars ahead
            %update road and car.position
            Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
            Cars{i}.position = [ Cars{i}.position(1) Cars{i}.position(2) (Cars{i}.position(3)+ direction*Cars{i}.velocity) ];
            Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
            Cars{i}.hasmoved = true;
            
            % What if there is a car in the cell in front & what if there is a car two cells in front, without a car 1 cell infront (move 1 cell)
            %%How to know if car will move in time step or already has moved.
            
            
        else %if car does exist in velocity or 1 ahead
            
            if(Cars{i}.velocity == 2)
                
                %if a car has velocity of 2, but can move 1 ahead, it does
                if(( Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3) + direction*Cars{i}.velocity) ~= 0) && (Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3) + direction*1) == 0))
                    
                    Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
                    Cars{i}.position = [ Cars{i}.position(1) Cars{i}.position(2) (Cars{i}.position(3)+ direction*1)];
                    Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
                    Cars{i}.hasmoved = true;
                    
                else
                    Cars{i}.wait = Cars{i}.wait + 1;
                    
                end
             
            else
                Cars{i}.wait = Cars{i}.wait + 1;
            end
                
            
            %look at car... NEED TO COMPLETE
            
        end
    else
        %if outside of system set the position to 0, set active to false
        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
        Cars{i}.position = [ 0 0 0 ];
        Cars{i}.active = false;
        Cars{i}.hasmoved = true;
        Cars{i}.wait = 0;
        
    end
    
    
    
%end
end


