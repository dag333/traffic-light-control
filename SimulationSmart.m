function [ Cars, Roads, Intersections, TIME, waitingtime, hasmoved, numactive] = SimulationSmart( Cars, Roads, Intersections, INIT_TIME, time, add_cars, maxcars )
%Simulation of Cars and Roads with SMARTLIGHTS

for c=1:length(Cars)
    %if(Cars{c}.active == true)
    Cars{c}.hasmoved = false;
    %end
end

CURR_TIME = INIT_TIME;
waitingtime = zeros(1,time);

for t=1:time
    
    hasmoved = 0;
    numactive = 0;
    
    CURR_TIME = CURR_TIME + 1;
    
    [ Intersections ] = SmartLights( Intersections, Cars, Roads );
    
    for i=1:length(Cars)
        
        if( Cars{i}.active == true) %check if car is in system
            
            if(atIntersection(Cars, Roads, i) ~= false) %check if car is at intersection
                %disp('first_if')
                [intersection_num, move_one] = atIntersection(Cars, Roads, i); %which intersection
                
                %figurig out what traffic light to look at
                car_road = Cars{i}.position(1);
                if(car_road == Intersections{intersection_num}.position(1))
                    light_num = 1;
                elseif(car_road == Intersections{intersection_num}.position(2))
                    light_num = 2;
                else
                    light_num = 0;
                    disp('Error');
                end
                
                if( Intersections{intersection_num}.light(light_num) == 1) %if green
                    %[Cars, Roads] = move(Cars, Roads, i); %move car
                    [ Cars, Roads ] = turn( Cars, Roads, Intersections, intersection_num, i );
                else
                    if(move_one == true) %situation where car has velocity of 2, but at red light, move up one into intersection
                        
                        if(Cars{i}.position(2) == 2) %if in lane 2, moves up
                            direction = -1;
                        elseif(Cars{i}.position(2) == 1) %if in lane 1, moves down
                            direction = 1;
                        end
                        
                        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
                        Cars{i}.position = [ Cars{i}.position(1) Cars{i}.position(2) (Cars{i}.position(3)+ direction*1)];
                        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
                        Cars{i}.hasmoved = true;
                    else
                        Cars{i}.hasmoved = true;
                    end
                    
                    Cars{i}.wait = Cars{i}.wait + 1;
                    
                end
            else
                %disp('move')
                [Cars, Roads] = move(Cars, Roads, i); %move car
                
            end
        end
    end
    
    %iterating through cars again to move cars that haven't moved yet
    for i=1:length(Cars)
        
        if( Cars{i}.active == true) %check if car is in system
            if(Cars{i}.hasmoved == false)
                if(atIntersection(Cars, Roads, i) ~= false) %check if car is at intersection
                    %disp('first_if')
                    intersection_num = atIntersection(Cars, Roads, i); %which intersection
                    
                    %figurig out what traffic light to look at
                    car_road = Cars{i}.position(1);
                    if(car_road == Intersections{intersection_num}.position(1))
                        light_num = 1;
                    elseif(car_road == Intersections{intersection_num}.position(2))
                        light_num = 2;
                    else
                        light_num = 0;
                        disp('Error');
                    end
                    
                    if( Intersections{intersection_num}.light(light_num) == 1) %if green
                        %[Cars, Roads] = move(Cars, Roads, i); %move car
                        [ Cars, Roads ] = turn( Cars, Roads, Intersections, intersection_num, i );
                    else
                        Cars{i}.hasmoved = true;
                    end
                    
                    Cars{i}.wait = Cars{i}.wait + 1;
                    
                else
                    %disp('move')
                    [Cars, Roads] = move(Cars, Roads, i); %move car
                    
                end
            end
        end
    end
    
    
    for m=1:length(Cars)
        if(Cars{m}.hasmoved == true)
            hasmoved = hasmoved + 1;
        end
        if(Cars{m}.active == true)
            numactive = numactive + 1;
            Cars{m}.tt = Cars{m}.tt + 1;
        end
    end
    
    %show visual
    %run('/Users/DanielGoldberg/Dropbox/Traffic Light Control/MATLAB/Visual2.m')
    
    [ wait ] = TotalWaitingTime( Cars );
    waitingtime(CURR_TIME) = wait;
    
    %uncomment if you want num cars waiting at each time step
    %     for i=1:length(Cars)
    %         Cars{i}.wait = 0;
    %     end
    
    if(add_cars == true)
        if(numactive < maxcars)
            add = maxcars - numactive;
            num_new = randi([1, add]);
            [ Cars ] = generateCars(Cars, Roads, num_new );
            
        end
    end
    
end

TIME = CURR_TIME;

end


