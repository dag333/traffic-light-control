function [ q, recur ] = Q( Cars, Roads, Intersections, i, pos, dest, light, recur )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

% pos = Cars{i}.position;
% dest = Cars{i}.dest;

gamma = .8; % 0 < gamma < 1

if( isequal(pos,dest) == 0)
    q = 0;
    
    %finds all possible new pos for car{i}
    [ new_ps ] = newPos( Cars, Roads, Intersections, pos, i, light );
    
    [rows, ~] = size(new_ps);
    
    for ps=1:rows
        
        recur = recur + 1;
        
        if(recur < 4)
            q = q + P( Cars, Roads, Intersections, pos, i, new_ps(ps,:), light)*(R(pos, new_ps(ps,:)) + gamma*V(Cars, Roads, Intersections, i, new_ps(ps,:), dest, recur));
        end
    end
    
    %for each new pos
    %calculate  q=q+P([tl,p,d],L,[tl?,p?])*(R([tl,p],[tl?,p?])+gamma * Vfunc_cal(car,tl?,p?,d?));
    
else
    q = 0;
end
end

