function [ Cars, Roads ] = turn( Cars, Roads, Intersections, intersection_num, i )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here



if(Cars{i}.map(intersection_num) == 0)
    
    %defining direction by lane number
    if(Cars{i}.position(2) == 2) %if in lane 2, moves down
        direction = -1;
    elseif(Cars{i}.position(2) == 1) %if in lane 1, moves up
        direction = 1;
    end
    
    if(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells((Cars{i}.position(3)+ direction*Cars{i}.velocity)) == 0)
        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
        Cars{i}.position = [ Cars{i}.position(1) Cars{i}.position(2) (Cars{i}.position(3)+ direction*Cars{i}.velocity) ];
        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
        Cars{i}.hasmoved = true;
  %      Cars{i}.wait = 0;
    elseif(Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells((Cars{i}.position(3)+ direction)) == 0)
        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
        Cars{i}.position = [ Cars{i}.position(1) Cars{i}.position(2) (Cars{i}.position(3)+ direction) ];
        Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
        Cars{i}.hasmoved = true;
  %      Cars{i}.wait = 0;    
    else
        Cars{i}.wait = Cars{i}.wait + 1;
    end
    
elseif(Cars{i}.map(intersection_num) == 1)
    
    %sprintf('turn 1 on Car %i', i)

    Road1 = Intersections{intersection_num}.position(1);
    Road2 = Intersections{intersection_num}.position(2);
    
    if(Cars{i}.position(1) == Road1)
       %sprintf('Road 1')
        
        if(Roads{Road2}.inter(2,1) == intersection_num)
            newcell = Roads{Road2}.inter(1,1);
        elseif(Roads{Road2}.inter(2,2) == intersection_num)
            newcell = Roads{Road2}.inter(1,2);
        else
            sprintf('Error: cannot find correct Intersection num')
        end
        
        
        if(Roads{Road2}.lane{1}.car_cells(newcell+1) == 0)
            Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
            Cars{i}.position = [ Road2 1 newcell+1];
            Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
            Cars{i}.hasmoved = true;
    %        Cars{i}.wait = 0;
        else
            Cars{i}.wait = Cars{i}.wait + 1;
        end
    else
        %sprintf('Road 2')
        
        if(Roads{Road1}.inter(2,1) == intersection_num)
                newcell = Roads{Road1}.inter(1,1);
        elseif(Roads{Road1}.inter(2,2) == intersection_num)
                newcell = Roads{Road1}.inter(1,2);
        else
            sprintf('Error: cannot find correct Intersection num')
        end
        
        if(Roads{Road1}.lane{1}.car_cells(newcell+1) == 0)
             Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
             Cars{i}.position = [ Road1 1 newcell+1];
             Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
             Cars{i}.hasmoved = true;
   %          Cars{i}.wait = 0;
        else
            Cars{i}.wait = Cars{i}.wait + 1;
        end
        
    end


elseif(Cars{i}.map(intersection_num) == -1)
    
   % sprintf('turn -1 on Car %i', i)
    
    Road1 = Intersections{intersection_num}.position(1);
    Road2 = Intersections{intersection_num}.position(2);

    
    if(Cars{i}.position(1) == Road1)
     %   sprintf('Road 1')
        
        if(Roads{Road2}.inter(2,1) == intersection_num)
                newcell = Roads{Road2}.inter(1,1);
        else
                newcell = Roads{Road2}.inter(1,2);
        end
        
        if(Roads{Road2}.lane{2}.car_cells(newcell-1) == 0)
             Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
             Cars{i}.position = [ Road2 2 newcell-1];
             Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
             Cars{i}.hasmoved = true;
 %            Cars{i}.wait = 0;
        else
            Cars{i}.wait = Cars{i}.wait + 1;
        end
    else
     %   sprintf('Road 2')
        
        if(Roads{Road1}.inter(2,1) == intersection_num)
                newcell = Roads{Road1}.inter(1,1);
     %           sprintf('if')
        else
                newcell = Roads{Road1}.inter(1,2);
     %           sprintf('else')
        end
        
        if(Roads{Road1}.lane{2}.car_cells(newcell-1) == 0)
            Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = 0;
            Cars{i}.position = [ Road1 2 newcell-1];
            Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;
            Cars{i}.hasmoved = true;
  %          Cars{i}.wait = 0;
        else
            Cars{i}.wait = Cars{i}.wait + 1;
        end
        
    end
    
else 
    
    sprintf('Bad Intersection Number! Index: %i', i )    

end

