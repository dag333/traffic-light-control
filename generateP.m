function [ P ] = generateP( Cars, Roads, Intersections )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

num_cars = length(Cars);
num_roads = length(Roads);
num_lanes = length(Roads{1}.lane);
num_cells = length(Roads{1}.lane{1}.car_cells);


a = .2;
b = 1-a;

c = .1;
d = .2;
e = 1 - c - d;

P = zeros(num_cars,num_roads, num_lanes, num_cells);

for i=1:num_cars
    
    if(Cars{i}.position == 1)
        direction = 1;
    else
        direction = -1;
    end
    
    
    %if car is not at an intersection
    if(atIntersection( Cars, Roads, i ) == 0)
        
        if(Cars{i}.velocity == 1)
            
            P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = a;
            P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)+direction) = b;
            
        elseif(Cars{i}.velocity == 2)
            
            P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = c;
            P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)+direction) = d;
            P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)+2*direction) = e;
        end
        
    %if car is at an intersection
    else
        inter_num = atIntersection( Cars, Roads, i );
        
        road = Cars{i}.position(1);
        
        %finding the status of the correct traffic light
        if(Intersections{inter_num}.position(1) == road)
            light = Intersections{inter_num}.light(1);
        elseif(Intersections{inter_num}.position(2) == road)
            light = Intersections{inter_num}.light(2);
        end
        
        %how the car with act at the current intersection
        movement = Cars{i}.map(inter_num);
        
       
        if(light == 0) %red
            
            P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = 1;
            
        else
            
            if(movement == 0)
                
                if(Cars{i}.velocity == 1)
                    
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = a;
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)+direction) = b;
                    
                elseif(Cars{i}.velocity == 2)
                    
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = c;
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)+direction) = d;
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)+2*direction) = e;
                end
                
            elseif(movement == -1)
                
                Road1 = Intersections{inter_num}.position(1);
                Road2 = Intersections{inter_num}.position(2);
                
                
                if(Cars{i}.position(1) == Road1)
                    
                    if(Roads{Road2}.inter(2,1) == inter_num)
                        newcell = Roads{Road2}.inter(1,1);
                    else
                        newcell = Roads{Road2}.inter(1,2);
                    end
                    
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = a;
                    P(i, Road2, 2, newcell-1) = b;
                    
                else
                    if(Roads{Road1}.inter(2,1) == inter_num)
                        newcell = Roads{Road1}.inter(1,1);
                    else
                        newcell = Roads{Road1}.inter(1,2);
                    end
                    
                    P(i, Cars{i}.position(1), Cars{i}.position(2),Cars{i}.position(3)) = a;
                    P(i, Road1, 2, newcell-1) = b;
                    
                end
                
            elseif(movement == 1)
                
                Road1 = Intersections{inter_num}.position(1);
                Road2 = Intersections{inter_num}.position(2);
                
                
                if(Cars{i}.position(1) == Road1)
                    
                    if(Roads{Road2}.inter(2,1) == inter_num)
                        newcell = Roads{Road2}.inter(1,1);
                    else
                        newcell = Roads{Road2}.inter(1,2);
                    end
                    
                    P(i,Cars{i}.position(1),Cars{i}.position(2),Cars{i}.position(3)) = a;
                    P(i, Road2, 1, newcell+1) = b;
                    
                else
                    if(Roads{Road1}.inter(2,1) == inter_num)
                        newcell = Roads{Road1}.inter(1,1);
                    else
                        newcell = Roads{Road1}.inter(1,2);
                    end
                    
                    P(i, Cars{i}.position(1), Cars{i}.position(2),Cars{i}.position(3)) = a;
                    P(i, Road1, 1, newcell+1) = b;
                    
                end
            end
            
            
        end
        
    end
    
end
end

