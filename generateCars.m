function [ Cars ] = generateCars(Cars, Roads, num_cars )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

for i=1:num_cars
    num = length(Cars) +1;
    
    Cars{num}.position = [1 1 0];
    
    enter_num = randi([1, 8]);
    
    pos = num2pos( enter_num );
    
    if(Roads{pos(1)}.lane{pos(2)}.car_cells(pos(3)) == 0)
        Cars{num}.position = num2pos( enter_num );
    else
        counter = 1;
        while(Roads{pos(1)}.lane{pos(2)}.car_cells(pos(3)) == 0)
            enter_num = randi([1, 8]);
            pos = num2pos( enter_num );
            %may need to rethink. what if car is in second position?
            if(counter > 8)
                return;
            end
        end
        Cars{num}.position = num2pos( enter_num );
    end
    
    
    Cars{num}.active = true;
    Cars{num}.color = [ceil(rand(1)*255)/255 ceil(rand(1)*255)/255 ceil(rand(1)*255)/255 ];
    
    Cars{num}.index = num;
    Cars{num}.velocity = ceil(2*rand(1));
    Cars{num}.hasmoved = false;
    Cars{num}.wait = 0; %wait time at current intersection
    Cars{num}.tt = 0; %traveling time
    
    dest_num = randi([1, 8]);
    Cars{num}.dest_num = dest_num;
    
    
    
    Cars{num}.dest = num2dest(Cars{num}.dest_num);
    Cars{num}.map = generateMap( Cars, num );
    
end
end

