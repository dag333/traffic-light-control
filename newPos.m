function [ new_ps ] = newPos( Cars, Roads, Intersections, pos, i, light )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here


new_ps = 0;

if(pos(2) == 1)
    direction = 1;
else
    direction = -1;
end


%if car is not at an intersection
if(atIntersectionSim( pos, Cars, Roads, i ) == 0)
    
    if(Cars{i}.velocity == 1)
        
        new_ps = [ pos; [pos(1) pos(2) pos(3)+direction]];
        
    elseif(Cars{i}.velocity == 2)
        
        new_ps = [ pos; [pos(1) pos(2) pos(3)+direction]; [pos(1) pos(2) pos(3)+2*direction] ];
    end
    
    %if car is at an intersection
else
    inter_num = atIntersectionSim( pos, Cars, Roads, i );
    
    road = pos(1);
    
    %     %finding the status of the correct traffic light
    %     if(Intersections{inter_num}.position(1) == road)
    %         light = Intersections{inter_num}.light(1);
    %     elseif(Intersections{inter_num}.position(2) == road)
    %         light = Intersections{inter_num}.light(2);
    %     end
    
    %how the car with act at the current intersection
    movement = Cars{i}.map(inter_num);
    
    
    if(light == 0) %red
        
        new_ps = [ pos ];
        
    else
        
        if(movement == 0)
            
            if(Cars{i}.velocity == 1)
                
                new_ps = [ pos; [pos(1) pos(2) pos(3)+direction]];
                
            elseif(Cars{i}.velocity == 2)
                
                new_ps = [ pos; [pos(1) pos(2) pos(3)+direction]; [pos(1) pos(2) pos(3)+2*direction] ];
                
            end
            
        elseif(movement == -1)
            
            Road1 = Intersections{inter_num}.position(1);
            Road2 = Intersections{inter_num}.position(2);
            
            
            if(Cars{i}.position(1) == Road1)
                
                if(Roads{Road2}.inter(2,1) == inter_num)
                    newcell = Roads{Road2}.inter(1,1);
                else
                    newcell = Roads{Road2}.inter(1,2);
                end
                
                new_ps = [ pos; [Road2 2 newcell-1]];
                
                
            else
                if(Roads{Road1}.inter(2,1) == inter_num)
                    newcell = Roads{Road1}.inter(1,1);
                else
                    newcell = Roads{Road1}.inter(1,2);
                end
                
                new_ps = [ pos; [Road1 2 newcell-1]];
                
            end
            
        elseif(movement == 1)
            
            Road1 = Intersections{inter_num}.position(1);
            Road2 = Intersections{inter_num}.position(2);
            
            
            if(Cars{i}.position(1) == Road1)
                
                if(Roads{Road2}.inter(2,1) == inter_num)
                    newcell = Roads{Road2}.inter(1,1);
                else
                    newcell = Roads{Road2}.inter(1,2);
                end
                
                
                new_ps = [ pos; [Road2 1 newcell+1]];
                
                
            else
                if(Roads{Road1}.inter(2,1) == inter_num)
                    newcell = Roads{Road1}.inter(1,1);
                else
                    newcell = Roads{Road1}.inter(1,2);
                end
                
                new_ps = [ pos; [Road1 1 newcell+1]];
                
                
            end
        end
        
        
    end
    
end

end

