function [ Cars, Roads, Intersections, TIME ] = generateModel(num_cars)
%generateModel creates structures to be used in traffic simulation
%   Cars, Roads, Intersections. Sets values.

num_inters = 4;

road_length = 10;

num_roads = 4;
num_lanes = 2;

TIME = 0;

%num_cars = num_cars;

Cars = cell(num_cars,1);

Roads = cell(num_roads,1);

Intersections = cell(num_inters,1);

for i=1:num_roads
    for j=1:num_lanes

Roads{i}.lane{j}.car_cells=zeros(10,1);
Roads{i}.lane{j}.index = j;

    end
end

for i=1:num_cars
    Cars{i}.position = [1 1 0];
    Cars{i}.active = true;
    Cars{i}.color = [ceil(rand(1)*255)/255 ceil(rand(1)*255)/255 ceil(rand(1)*255)/255 ];
end

for i=1:num_cars
    
%ASUMPTION: Every road has same length, same number of lanes
r = ceil(rand(1)*num_roads);
l = ceil(rand(1)*num_lanes);
c = ceil(rand(1)*road_length);
    
Cars{i}.index = i;
Cars{i}.velocity = ceil(2*rand(1));
Cars{i}.hasmoved = false; 
Cars{i}.wait = 0; %wait time at current intersection
Cars{i}.tt = 0; %traveling time

dest_num = randi([1, 8]);
Cars{i}.dest_num = dest_num;


    if(Roads{r}.lane{l}.car_cells(c) == 0)
         Cars{i}.position = [r l c];
    else
        %i = i-1; 
        %i=k;%WRONG
        %while the next one isn't empty, keep checking... out of while loop, put the car in that cell
        Cars{i}.position = [r l c+1]; %may be a bad idea
    end
end

for i = 1:num_cars

Roads{Cars{i}.position(1)}.lane{Cars{i}.position(2)}.car_cells(Cars{i}.position(3)) = Cars{i}.index;    
    
end

Roads{1}.inter = [ 3 8; 1 2]; %cell 3 = Inter 1, cell 8 = Inter 2
Roads{2}.inter = [ 3 8; 4 3];
Roads{3}.inter = [ 3 8; 1 4];
Roads{4}.inter = [ 3 8; 2 3];

Intersections{1}.position = [1 3]; %[Road 1 Road 3]
Intersections{2}.position = [1 4];
Intersections{3}.position = [2 4];
Intersections{4}.position = [2 3];

%Intersection of each lane (1=Green, 0=Red)
Intersections{1}.light = [0 1]; %Road1, Road2, turn1, turn2 (turns not included in 1 lane system)
Intersections{2}.light = [0 1];
Intersections{3}.light = [0 1];
Intersections{4}.light = [0 1];

Intersections{1}.index = 1;
Intersections{2}.index = 2;
Intersections{3}.index = 3;
Intersections{4}.index = 4;


for i = 1:num_cars
   
   Cars{i}.dest = num2dest(Cars{i}.dest_num);
   Cars{i}.map = generateMap( Cars, i );
    
end


%show visual
%run('/Users/DanielGoldberg/Dropbox/Traffic Light Control/MATLAB/Visual2.m')  

end




