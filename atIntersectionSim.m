function [ output ] = atIntersectionSim( pos, Cars, Roads, i )
%intersection Takes in cars and current index
%   return which intersection car is at, if none returns 0.

output = false;

if(pos(2) == 2) %if in lane 2, moves up
    direction = 1;
elseif(pos(2) == 1) %if in lane 1, moves down
    direction = -1;
end


int_cell_1 = Roads{pos(1)}.inter(1,1);
int_cell_2 = Roads{pos(1)}.inter(1,2);



if( Roads{pos(1)}.inter(1,1) == pos(3)) 
     output = Roads{pos(1)}.inter(2,1);   
    
elseif(Roads{pos(1)}.inter(1,2) == pos(3))
    output = Roads{pos(1)}.inter(2,2);
    
elseif((Cars{i}.velocity == 2)&&(Roads{pos(1)}.lane{pos(2)}.car_cells(int_cell_1) == 0)&&(Roads{pos(1)}.lane{pos(2)}.car_cells(int_cell_1+direction) == Cars{i}.index))    
    output = Roads{pos(1)}.inter(2,1);

elseif((Cars{i}.velocity == 2)&&(Roads{pos(1)}.lane{pos(2)}.car_cells(int_cell_2) == 0)&&(Roads{pos(1)}.lane{pos(2)}.car_cells(int_cell_2+direction) == Cars{i}.index))    
    output = Roads{Cars{i}.position(1)}.inter(2,2);
    
end