function [ Intersections ] = changeLights( TIME, Intersections, alt_time )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

 %simple way change/alter the traffic lights 
    
  % if(counter < 3)
  %      counter = counter + 1;
  % else
  %      for L=1:length(Intersections) 
  %          if(Intersections{L}.light == [1 1 1])
  %               Intersections{L}.light = [0 0 0];
  %          else
  %               Intersections{L}.light = [1 1 1];
  %          end
  %      end
  %      counter = 1;
  % end
  
  x = alt_time*2;

  status = round(mod(TIME-1,x)/x); %green = 1, red = 0
  
  for i=1:length(Intersections)
     if( status == 1)   
       Intersections{i}.light = [1 0];    
     else
       Intersections{i}.light = [0 1];  
     end
  end
  
      
  
  

end

