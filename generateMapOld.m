function [ map ] = generateMapOld( Cars, i )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

nextInter = nextIntersection(Cars{i}.position);

map = [0 0 0 0];

if(nextInter == 1)
    curr_x = 1;
    curr_y = 2;
elseif(nextInter == 2)
    curr_x = 2;
    curr_y = 2;
elseif(nextInter == 3)
    curr_x = 2;
    curr_y = 1;
elseif(nextInter == 4)
    curr_x = 1;
    curr_y = 1;
else
    curr_x = 0;
    curr_y = 0;
end

if(curr_x ~= 0)
    [dest_x, dest_y] = findDest(Cars, i);
    %finds destination index on x,y coordinate plane
    
    x = dest_x - curr_x;
    y = dest_y - curr_y;
    
    
    while((x ~= 0) || (y~=0))
        p = round(rand()); % random 0 or 1
        
        if(p == 0)
            if(x > 0)
                inter = xyInt(curr_x,curr_y);
                
                x = x - 1;
                curr_x = curr_x + 1;
                
                %          Cars{i}.map(Int) = 1;
                
                map(inter) = 1;
                
            elseif( x < 0 )
                inter = xyInt(curr_x,curr_y);
                
                x = x + 1;
                curr_x = curr_x - 1;
                
                map(inter) = -1;
                
            else %if x==0  usure if this is needed??
              %  inter = xyInt(curr_x,curr_y);
                
              %  map(inter) = 0;
                
            end
        else  %if p == 1
            if(y > 0)
                inter = xyInt(curr_x,curr_y);
                
                y = y - 1;
                curr_y = curr_y + 1;
                
                %          Cars{i}.map(Int) = 1;
                
                map(inter) = 1;
                
            elseif( y < 0 )
                inter = xyInt(curr_x,curr_y);
                
                y = y + 1;
                curr_y = curr_y - 1;
                
                
                map(inter) = -1;
                
            else %if y==0   also unsure if this is needed
              %  inter = xyInt(curr_x,curr_y);
                
              %  map(inter) = 0;
            end
            
            
        end
        
    end
    
    
end
end