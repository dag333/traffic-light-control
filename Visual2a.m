figure;

plot([6;6],[0;30]);hold on;
plot([8;8],[0;30], '--k');hold on;
plot([10;10],[0;30]);hold on;
plot([20;20],[0;30]);hold on;
plot([22;22],[0;30], '--k');hold on;
plot([24;24],[0;30]);hold on;

plot([0;30],[6;6]);hold on;
plot([0;30],[8;8], '--k');hold on;
plot([0;30],[10;10]);hold on;
plot([0;30],[20;20]);hold on;
plot([0;30],[22;22], '--k');hold on;
plot([0;30],[24;24]);hold on;

for i=1:length(Intersections)
   
   if(Intersections{i}.index == 1)
       if(Intersections{i}.light(1) == 1) %street1
            plot(6,23,'MarkerFaceColor','g','Marker','o')
            plot(10,21,'MarkerFaceColor','g','Marker','o')
       else
            plot(6,23,'MarkerFaceColor','r','Marker','o')
            plot(10,21,'MarkerFaceColor','r','Marker','o')
       end
       if(Intersections{i}.light(2) == 1) %street2
            plot(7,20,'MarkerFaceColor','g','Marker','o')
            plot(9,24,'MarkerFaceColor','g','Marker','o')
       else
            plot(7,20,'MarkerFaceColor','r','Marker','o')
            plot(9,24,'MarkerFaceColor','r','Marker','o')
       end
%        if(Intersections{i}.light(3) == 1) %right turn
%             plot(8,20,'MarkerFaceColor','g','Marker','o')
%             plot(8,24,'MarkerFaceColor','g','Marker','o')
%        else
%             plot(8,20,'MarkerFaceColor','r','Marker','o')
%             plot(8,24,'MarkerFaceColor','r','Marker','o')
%        end
   elseif(Intersections{i}.index == 2)
       if(Intersections{i}.light(1) == 1) %street1
            plot(20,23,'MarkerFaceColor','g','Marker','o')
            plot(24,21,'MarkerFaceColor','g','Marker','o')
       else
            plot(20,23,'MarkerFaceColor','r','Marker','o')
            plot(24,21,'MarkerFaceColor','r','Marker','o')
       end
       if(Intersections{i}.light(2) == 1) %street2
            plot(21,20,'MarkerFaceColor','g','Marker','o')
            plot(23,24,'MarkerFaceColor','g','Marker','o')
       else
            plot(21,20,'MarkerFaceColor','r','Marker','o')
            plot(23,24,'MarkerFaceColor','r','Marker','o')
       end
%        if(Intersections{i}.light(3) == 1) %right turn
%             plot(22,20,'MarkerFaceColor','g','Marker','o')
%             plot(22,24,'MarkerFaceColor','g','Marker','o')
%        else
%             plot(22,20,'MarkerFaceColor','r','Marker','o')
%             plot(22,24,'MarkerFaceColor','r','Marker','o')
%        end
   elseif(Intersections{i}.index == 3)
       if(Intersections{i}.light(1) == 1) %street1
            plot(6,9,'MarkerFaceColor','g','Marker','o')
            plot(10,7,'MarkerFaceColor','g','Marker','o')
       else
            plot(6,9,'MarkerFaceColor','r','Marker','o')
            plot(10,7,'MarkerFaceColor','r','Marker','o')
       end
       if(Intersections{i}.light(2) == 1) %street2
            plot(7,6,'MarkerFaceColor','g','Marker','o')
            plot(9,10,'MarkerFaceColor','g','Marker','o')
       else
            plot(7,6,'MarkerFaceColor','r','Marker','o')
            plot(9,10,'MarkerFaceColor','r','Marker','o')
       end
   elseif(Intersections{i}.index == 4)
      if(Intersections{i}.light(1) == 1) %street1
            plot(20,9,'MarkerFaceColor','g','Marker','o')
            plot(24,7,'MarkerFaceColor','g','Marker','o')
       else
            plot(20,9,'MarkerFaceColor','r','Marker','o')
            plot(24,7,'MarkerFaceColor','r','Marker','o')
       end
       if(Intersections{i}.light(2) == 1) %street2
            plot(21,6,'MarkerFaceColor','g','Marker','o')
            plot(23,10,'MarkerFaceColor','g','Marker','o')
       else
            plot(21,6,'MarkerFaceColor','r','Marker','o')
            plot(23,10,'MarkerFaceColor','r','Marker','o')
       end
   end
end

for i=1:length(Cars)
    
    if(Cars{i}.active == true)
        
        Road = Cars{i}.position(1);
        Lane = Cars{i}.position(2);
        Cell = Cars{i}.position(3);
        
        
        if(Road == 1)
          if(Lane == 1) 
            y = 23;
            
            if( Cell <= 3)
                
                x = Cell*2 - 1;
            
            elseif( Cell > 3 && Cell <= 8)
                
                x =  Cell*2 + 3;
            else
                x =  Cell*2 + 7;
            end
          else
             y = 21; 
             
             if( Cell <= 2)
                 
                x = Cell*2 + 1;
                
            elseif( Cell > 2 && Cell <= 7)
                
                x =  Cell*2 + 5;
                
            else
                x =  Cell*2 + 9;
            end
               
          end
        elseif(Road == 2)
         if(Lane == 1) 
            y = 9;
            
            if( Cell <= 3)
                
                x = Cell*2 - 1;
            
            elseif( Cell > 3 && Cell <= 8)
                
                x =  Cell*2 + 3;
            else
                x =  Cell*2 + 7;
            end
          else
             y = 7; 
             
             if( Cell <= 2)
                 
                x = Cell*2 + 1;
                
            elseif( Cell > 2 && Cell <= 7)
                
                x =  Cell*2 + 5;
                
            else
                x =  Cell*2 + 9;
            end
               
          end
        elseif(Road == 3)
        
         if(Lane == 1) 
            x = 9; 
            
            if( Cell <= 3)
                
                y = 30 - Cell*2 + 1;
            
            elseif( Cell > 3 && Cell <= 8)
                
                y = 30 - Cell*2 -3;
            else
                y = 30 - Cell*2 - 8;
            end
          else
             x = 7; 
             
             if( Cell <= 2)
                 
                y = 30 - Cell*2;
                
            elseif( Cell > 2 && Cell <= 7)
                
                y =  30 - Cell*2 - 5;
                
            else
               y =  30 - Cell*2 - 7;
            end
               
          end
           
        else
          if(Lane == 1) 
            x = 23; 
            
            if( Cell <= 3)
                
                y = 30 - Cell*2 + 1;
            
            elseif( Cell > 3 && Cell <= 8)
                
                y = 30 - Cell*2 -3;
            else
                y = 30 - Cell*2 - 8;
            end
          else
             x = 21; 
             
             if( Cell <= 2)
                 
                y = 30 - Cell*2;
                
            elseif( Cell > 2 && Cell <= 7)
                
                y =  30 - Cell*2 - 5;
                
            else
               x =  30 - Cell*2 - 7;
            end
               
          end
        end
        
        plot(x,y,'MarkerFaceColor', Cars{i}.color,'MarkerSize', 10, 'Marker','s'); hold on;
        
        %%Attempt at Legend to match color with car num
        %h(i) = plot(x,y,'MarkerFaceColor', Cars{i}.color,'MarkerSize', 10, 'Marker','s'); hold on;
        %name{i} = ['condition ', num2str(i)];  
        
    end
end

%legend(h, name);


